# Other data

No wiki for this topic but in addition to the data proposed for the Hackathon, you can search for datasets on the following sites :


* HDX : https://data.humdata.org/group/cmr

* MINFOF : http://cmr-data.forest-atlas.org/

* Knoema : https://knoema.fr/atlas/Cameroun

* IPCC : http://www.ipcc-data.org/

* MINADER : http://cameroon.countrystat.org/search-and-visualize-data/en/

* Healthsites : https://healthsites.io/#country-data

* Geofabrik (OSM) : http://download.geofabrik.de/africa/cameroon.html

* Ressourcewatch : https://resourcewatch.org/data/explore?zoom=7&lat=5.287887414011332&lng=12.821044921875002&basemap=dark&labels=light&page=1&sort=most-viewed&sortDirection=-1&topics=%255B%2522food_and_agriculture%2522%255D

* Landmarkmap : http://www.landmarkmap.org/map/#x=-102.46&y=13.47?x=12.79&y=3.94&z=9&l=en

* Food and agriculture organisation of the United Nation: Resilience (http://www.fao.org/resilience/home/en/)

#### Agriculture and climate change
* Adapting agriculture of climate change : infography (in French) http://www.fao.org/3/a-i6398f.pdf 
* Agriculture and forest, [solutions to respond to climate change](http://draaf.normandie.agriculture.gouv.fr/IMG/pdf/Notedesynthese-RAP-CGAAER-Changement-climatique_cle0a523e_cle019551.pdf) (in French) 

#### Agriculture, landscape and ecosystem service
* [Agrobiodiversity Index Report](https://cgspace.cgiar.org/bitstream/handle/10568/100820/BookIndex_RiskResilience_01Ago_LOW.pdf?sequence=11&isAllowed=y) 

The report brings together data on dimensions of agrobiodiversity in ten countries to measure food system sustainability and resilience.  

* [Productive agricultural ecosystems](https://www.bioversityinternational.org/research-portfolio/agricultural-ecosystems/)
* [Landscape and ecosystem services](https://www.bioversityinternational.org/research-portfolio/agricultural-ecosystems/landscapes-and-ecosystems/)

#### FAO
* [Cameroon : Cartes historiques des sols et des terrains](www.fao.org/soils-portal/soil-survey/cartes-historiques-et-bases-de-donnees-des-sols/cartes-historiques-des-sols-et-des-terrains-de-la-fao/fr/)
* [Cameroon : Global Terrain Slope and Aspect Data](http://www.fao.org/soils-portal/soil-survey/cartes-historiques-et-bases-de-donnees-des-sols/base-harmonisee-mondiale-de-donnees-sur-les-sols-version-12/fr/)

#### Biodiversity
* [The Scientific Landscape for Access and Benefit-Sharing in Cameroon](https://public.tableau.com/profile/poldham#!/vizhome/Cameroon_0/Cameroon)

#### IPIS - International Peace Information Service
* [Conflict mapping - Maps and Data](http://ipisresearch.be/home/conflict-mapping/maps/open-data/)

#### CITES
* [MIKE - Monitoring illegal Killing Elephants](https://www.cites.org/eng/prog/mike/index.php#MIKE%20Data%20Analysis)

</br>
</br>

**If you use additional datasets, make sure that the user licences allow it.**

**Also verify that the date of the last update is sufficiently recent to make the dataset viable.**